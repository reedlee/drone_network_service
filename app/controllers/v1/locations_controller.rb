module V1
  class LocationsController < ApiController

    def new
      json = {
          x: "float number",
          y: "float number",
          z: "float number",
          vel: "positive float number",
          required: ["x", "y", "z", "vel"]
      }
      render json: json, status: 200
    end

    def calculate
      location = Location.new(Rule.new)
      begin
        extended_params = calc_params.merge!({"sector_id" => Sector::ID})
        position = location.calculate(extended_params)
        json = {loc: position}
        render json: json, status: 200
      rescue LocationError => error
        render json: {message: error.message}, status: 400
      end
    end

    private
    def calc_params
      params.permit(:x, :y, :z, :vel)
    end
  end
end