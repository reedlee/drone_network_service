Rails.application.routes.draw do
  root to: 'health_checker#index'

  namespace :v1 do
    get 'location/calculate', to: 'locations#new'
    post 'location/calculate', to: 'locations#calculate'
  end
end
