# drone_network_service
[Wiki page of the project](https://gitlab.com/reedlee/drone_network_service/wikis/home)
## How to run application
1. You need to install docker and docker-compose: https://docs.docker.com/compose/install
2. You need to clone application from git: `git clone git@gitlab.com:reedlee/drone_network_service.git`
3. Than you need to download/build images: `docker-compose build`
4. You need to use `docker-compose up` to start the application
5. You need to use `docker-compose run web rspec -f documentation` to run tests 

