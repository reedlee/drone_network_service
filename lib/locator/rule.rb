class RuleError < StandardError; end
class RuleErrorNegativeVelocity < RuleError; end
class RuleErrorParamNotExisted < RuleError; end
class RuleErrorNotFloatNumber < RuleError; end

class Rule
  def initialize
    @template_params = {
        x: nil,
        y: nil,
        z: nil,
        vel: nil,
        sector_id: nil
    }
  end

  def calculate(params)
    parse(params)
    validate(params)
    (@template_params[:sector_id]*(@template_params[:x].to_f + @template_params[:y].to_f + @template_params[:z].to_f) + @template_params[:vel].to_f).round(2)
  end

  def parse(params)
    @template_params.keys.each do |param|
      if @template_params[param].nil?
        @template_params[param]= params[param]
      end
    end
  end

  def validate(params)
    all_params_is_exist?
    all_params_is_float?
    velocity_is_positive?(params[:vel].to_f)
  end

  def all_params_is_exist?
    @template_params.each do |param, value|
      raise RuleErrorParamNotExisted.new("Validation failed: Param #{param} isn't existed") if value.nil?
    end
  end

  def all_params_is_float?
    @template_params.each do |param, value|
       !!Float(value) rescue raise RuleErrorNotFloatNumber.new("Validation failed: Param #{param} isn't float")
    end
  end

  def velocity_is_positive?(velocity)
    raise RuleErrorNegativeVelocity.new("Validation failed: Param vel is negative") if velocity < 0
  end
end