class LocationError < StandardError; end

class Location
  def initialize(rule)
    @rule = rule
  end

  def calculate(params)
    begin
      @rule.calculate(params)
    rescue RuleError => error
      raise LocationError.new(error.message)
    end
  end
end
