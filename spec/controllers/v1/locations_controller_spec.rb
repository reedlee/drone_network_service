require 'rails_helper'

RSpec.describe V1::LocationsController, type: :controller do

  context 'drone requests the list of parameters to calculate a storage location' do
    it 'get success request' do
      get :new

      expect(response.status).to eql(200)
      expect(response_body_to_json).to eql(
                                           {
                                               x: "float number",
                                               y: "float number",
                                               z: "float number",
                                               vel: "positive float number",
                                               required: ["x", "y", "z", "vel"]
                                           }
                                           .as_json)
    end
  end

  context 'drone requests location by coords and velocity' do
    let(:params) {
      {
          x: "123.12",
          y: "456.56",
          z: "789.89",
          vel: "20.0"
      }
    }
    it 'post success response with location' do
      post :calculate, params: params

      expect(response.status).to eql(200)
      expect(response_body_to_json).to eql({loc: 1389.57}.as_json)
    end

    it 'invalid data' do
      params[:vel] = -20
      post :calculate, params: params

      expect(response.status).to eql(400)
      expect(response_body_to_json).to eql({message: "Validation failed: Param vel is negative"}.as_json)
    end
  end
end
