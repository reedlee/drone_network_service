require 'rails_helper'

RSpec.describe HealthCheckerController, type: :controller do
  context 'a drone checks DNS' do
    it 'OK' do
      get :index

      expect(response.status).to eql(200)
      expect(response_body_to_json).to eql({status: "OK"}.as_json)
    end
  end
end
