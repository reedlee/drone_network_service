require 'rails_helper'

describe Rule do

  let(:params){
    params = {
        x: "123.12",
        y: "456.56",
        z: "789.89",
        vel: "20.0",
        sector_id: 1
    }
  }

  it 'should calculate' do
    rule = Rule.new
    location = rule.calculate(params)

    expect(location).to eql(1389.57)
  end

  context 'should raise error' do
    it 'RuleErrorNegativeVelocity' do
      rule = Rule.new
      params[:vel] = "-20"

      expect {rule.calculate(params)}.to raise_error(RuleErrorNegativeVelocity, /Validation failed: Param vel is negative/)
    end

    it 'RuleErrorNotFloatNumber' do
      rule = Rule.new
      params[:x] = "aaaa"

      expect {rule.calculate(params)}.to raise_error(RuleErrorNotFloatNumber, /Validation failed: Param x isn't float/)
    end

    it 'RuleErrorParamNotExisted' do
      rule = Rule.new
      params = {
          y: "456.56",
          z: "789.89",
          vel: "20.0",
      }

      expect {rule.calculate(params)}.to raise_error(RuleErrorParamNotExisted, /Validation failed: Param x isn't existed/)
    end
  end
end